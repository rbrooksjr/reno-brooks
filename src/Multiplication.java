import java.util.Scanner;

public class Multiplication {


    public static void main(String[] args) {
        Scanner input= new Scanner ( System.in );
        
        int x;
        int y;
        int z;
        int result;
        
        System.out.print("Where the first number");
        x=input.nextInt();
        
        System.out.print("Where the second number");
        y=input.nextInt();
        
        System.out.print("Where the third number");
        z= input.nextInt();
        
        result=x*y*z;
        
        System.out.printf("The product of %d, %d, and %d is %d\n", 
                x, y, z, result);
        
    }
}
